import os
import uuid
import tarfile

import pytest

from bitbucket_pipes_toolkit.test import PipeTestCase


class HerokuDeployTestCase(PipeTestCase):

    def setUp(self):
        self.build_id = uuid.uuid4()
        self.dirname = os.path.dirname(__file__)

        with tarfile.open(os.path.join(self.dirname, 'sample-app.tar.gz'), "w:gz") as tar:
            source_dir = os.path.join(self.dirname, 'python-getting-started-master')
            tar.add(source_dir, arcname=os.path.basename(source_dir))

    def tearDown(self):
        os.remove(os.path.join(self.dirname, 'sample-app.tar.gz'))

    def test_no_parameters(self):
        result = self.run_container()
        self.assertRegex(result, 'Validation errors: \nHEROKU_API_KEY:\n- required field')

    @pytest.mark.last
    def test_app_deployed_successfully_default(self):
        app_name = os.getenv('HEROKU_APP_NAME')
        result = self.run_container(environment={
            'HEROKU_API_KEY': os.getenv('HEROKU_API_KEY'),
            'HEROKU_APP_NAME': app_name,
            'ZIP_FILE': 'test/sample-app.tar.gz'
        })
        self.assertRegex(result, 'Successfully started a new build')
        self.assertNotIn('BEGIN streaming', result)

    def test_app_deployed_successfully_wait(self):
        result = self.run_container(environment={
            'HEROKU_API_KEY': os.getenv('HEROKU_API_KEY'),
            'HEROKU_APP_NAME': os.getenv('HEROKU_APP_NAME'),
            'ZIP_FILE': 'test/sample-app.tar.gz',
            'WAIT': True
        })
        self.assertRegex(result, 'Successfully deployed the application to Heroku')

    def test_app_deployment_fails_with_invalid_package(self):

        result = self.run_container(environment={
            'HEROKU_API_KEY': os.getenv('HEROKU_API_KEY'),
            'HEROKU_APP_NAME': os.getenv('HEROKU_APP_NAME'),
            'ZIP_FILE': 'test/invalid-package.tar.gz',
            'WAIT': True,
        })
        self.assertRegex(result, 'Heroku build failed!')

    def test_deployment_fails_if_app_access_denied(self):

        app_name = 'pipes-ci-aaccess-denied'

        result = self.run_container(environment={
            'HEROKU_API_KEY': os.getenv('HEROKU_API_KEY'),
            'HEROKU_APP_NAME': app_name,
            'ZIP_FILE': 'test/invalid-package.tar.gz',
            'WAIT': True,
        })

        self.assertRegex(result, f'You do not have access to the app {app_name}')
